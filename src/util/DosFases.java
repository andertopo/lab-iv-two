/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Vector;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import simuladorinvestigacionoperaciones.DosFasesFrame;

/**
 *
 * @author atorrepo
 */
public class DosFases {

    private DefaultTableModel modeloConRestricciones;
    private Vector<Fraccion> funcObjConRestricciones;
    private Vector<Fraccion> funcObjConRestriccionesPrimeraFase;
    int columnasRestricciones;
    int columnaRatio;
    int columnaOperacion;
    private int filaPivote;
    private int columnaPivote;
    private int iteracion;
    private DosFasesFrame dosFasesFrame;

    public DosFases(DosFasesFrame dosFasesFrame) {
        this.filaPivote = 1;//se inicializan por si las moscas
        this.columnaPivote = 3;//se inicializan por si las moscas
        this.columnasRestricciones = 0;//se inicializan por si las moscas
        this.columnaRatio = 0;//se inicializan por si las moscas
        this.columnaOperacion = 0;//se inicializan por si las moscas
        this.iteracion = 0;
        this.modeloConRestricciones = new DefaultTableModel();
        this.funcObjConRestricciones = new Vector<>();
        this.dosFasesFrame = dosFasesFrame;
    }
    
    /**
     * Se calculan los valores para las filas Zj (multiplicacion de las
     * variables base y los coeficientes de las restricciones sumadas) y Cj - Zj
     * (coeficientes de la funcion objetivo menos Zj)
     */
    public void calcularZj() {
        try {
            int filas = (this.iteracion == 1) ? (this.modeloConRestricciones.getRowCount() - 1): (this.modeloConRestricciones.getRowCount() - 3);
            Object[] Zjs = new Object[this.columnasRestricciones + 3];//se deja en columnas -2 porque la última columna se deja para el ratio
            Object[] CjZjs = new Object[this.columnasRestricciones + 3];//se deja en columnas -2 porque la última columna se deja para el ratio
            for (int j = 3; j < Zjs.length; j++) {// se inicializan Zj y CjZj
                Zjs[j] = new Fraccion("0");
                CjZjs[j] = new Fraccion(this.modeloConRestricciones.getValueAt(0, j).toString());
            }
            Zjs[0] = "";
            Zjs[1] = "";
            Zjs[2] = "Zj";

            CjZjs[0] = "";
            CjZjs[1] = "";
            CjZjs[2] = "Cj - Zj";
            for (int i = 1; i <= filas; i++) {//se recorren filas
                for (int j = 3; j < this.columnasRestricciones + 3; j++) {//se deja en columnas -1 porque la última columna se deja para el ratio
                    //Double zj = Double.parseDouble(this.modeloConRestricciones.getValueAt(i, 1).toString()) * Double.parseDouble(this.modeloConRestricciones.getValueAt(i, j).toString());
                    Fraccion Cb = (Fraccion) this.modeloConRestricciones.getValueAt(i, 1);
                    System.out.println("Cb de " + i + " - " + j + ": " + Cb);
                    Fraccion restriccion = new Fraccion(this.modeloConRestricciones.getValueAt(i, j).toString());
                    System.out.println("restriccion de " + i + " - " + j + ": " + restriccion);
                    Fraccion zj = Cb.multiplica(restriccion);
                    System.out.println("Zj " + zj);
                    System.out.println("Zjs antes: " + Zjs[j]);
                    Zjs[j] = new Fraccion(Zjs[j].toString()).sumar(zj);
                    System.out.println("Zjs Despues: " + Zjs[j]);
                }
            }
            for (int j = 3; j < this.columnasRestricciones + 3; j++) {//se deja en columnas -1 porque la última columna se deja para el ratio
                System.out.println("Columna " + this.modeloConRestricciones.getColumnName(j) + " -- " + Zjs[j].toString() + " -- " + CjZjs[j].toString());
                CjZjs[j] = new Fraccion(Zjs[j].toString()).restar(new Fraccion(CjZjs[j].toString()));
                System.out.println("\n\n\n");
            }
            if(this.iteracion == 1) {
                this.modeloConRestricciones.addRow(Zjs);
                this.modeloConRestricciones.addRow(CjZjs);
            } else {
                this.modeloConRestricciones.removeRow(this.modeloConRestricciones.getRowCount() - 1);
                this.modeloConRestricciones.removeRow(this.modeloConRestricciones.getRowCount() - 1);
                this.modeloConRestricciones.addRow(Zjs);
                this.modeloConRestricciones.addRow(CjZjs);
                //this.modeloConRestricciones.insertRow(this.modeloConRestricciones.getRowCount() - 2, Zjs);
                //this.modeloConRestricciones.insertRow(this.modeloConRestricciones.getRowCount() - 1, CjZjs);
            }
        } catch (Exception e) {
            System.err.println("Error calcularZj: " + e.toString());
            e.printStackTrace();
        }
    }

    public int hayarColumnaPivote(String metodo) {
        this.columnaPivote = 3;
        Fraccion valorColumnaPivote = new Fraccion("0");
        int filaCjZj = this.modeloConRestricciones.getRowCount() - 1;
        for (int j = (this.columnasRestricciones + 2); j > 2; j--) {
            Fraccion valorColumnaPivoteTemp = (Fraccion) this.modeloConRestricciones.getValueAt(filaCjZj, j);
            System.out.println("J " + j + " -- " + valorColumnaPivoteTemp);
            if(metodo.equals("MIN")) {
                if (valorColumnaPivoteTemp.esMayorQue(0.0)) {
                    if (valorColumnaPivoteTemp.esMayorIgualQue(valorColumnaPivote)) {
                        this.columnaPivote = j;
                        valorColumnaPivote = valorColumnaPivoteTemp;
                    }
                }
            } else if(metodo.equals("MAX")) {
                if (valorColumnaPivoteTemp.esMenorQue(0.0)) {
                    if (valorColumnaPivoteTemp.esMenorIgualQue(valorColumnaPivote)) {
                        this.columnaPivote = j;
                        valorColumnaPivote = valorColumnaPivoteTemp;
                    }
                }
            }
        }
        return this.columnaPivote;
    }

    public int hayarFilaPivote(int columnaPivote, String fase) {
        this.filaPivote = 1;
        Fraccion cociente = new Fraccion();
        Fraccion primerValor = new Fraccion(this.modeloConRestricciones.getValueAt(1, columnaPivote).toString());
        Fraccion cocienteAcumulado = new Fraccion("0");
        if(!primerValor.toString().equals("0")) {
                Fraccion valorVariableBase = new Fraccion(this.modeloConRestricciones.getValueAt(1, 2).toString());
                cocienteAcumulado = valorVariableBase.divide(primerValor);
        } else {
            this.modeloConRestricciones.setValueAt("Div x Cero", 1, this.columnaRatio);
        }
        for (int i = 1; i < this.modeloConRestricciones.getRowCount() - 2; i++) {
            System.out.println("valor fila de ColumnaPivote:  " + this.modeloConRestricciones.getValueAt(i, columnaPivote) + " -- " + this.modeloConRestricciones.getValueAt(i, 2));
            Fraccion valorFilaDeColumaPivote = new Fraccion(this.modeloConRestricciones.getValueAt(i, columnaPivote).toString());
            Fraccion condicion = new Fraccion(this.modeloConRestricciones.getValueAt(i, 2).toString());
            if (!valorFilaDeColumaPivote.toString().equals("0")) {
                cociente = condicion.divide(valorFilaDeColumaPivote);
                if ( cociente.esMenorIgualQue(cocienteAcumulado)) {
                    cocienteAcumulado = cociente;
                    this.filaPivote = i;
                }
            } else {
                this.modeloConRestricciones.setValueAt("Div x Cero", i, this.columnaRatio);
                continue;
            }
            this.modeloConRestricciones.setValueAt(cociente, i, this.columnaRatio);
        }
        this.setVariableEntrante(fase);
        
        return this.filaPivote;
    }
    
    private void setVariableEntrante(String fase) {
        String variableSale = this.modeloConRestricciones.getValueAt(this.filaPivote, 0).toString();
        String valueSale = this.modeloConRestricciones.getValueAt(this.filaPivote, 1).toString();
        this.dosFasesFrame.setVariableSale(variableSale, valueSale, fase);
        
        //se cambia la variabla de base inicial (la que esté en la fila pivote), por la variable que está en la columna pivote
        this.modeloConRestricciones.setValueAt(this.modeloConRestricciones.getColumnName(this.columnaPivote), filaPivote, 0);
        //se cambia el valor de base inicial (la que esté en la fila pivote), por el valor que está en la columna pivote
        this.modeloConRestricciones.setValueAt(this.modeloConRestricciones.getValueAt(0, this.columnaPivote), filaPivote, 1);
        String variableEntra = this.modeloConRestricciones.getValueAt(this.filaPivote, 0).toString();
        String valueEntra = this.modeloConRestricciones.getValueAt(this.filaPivote, 1).toString();
        this.dosFasesFrame.setVariableEntra(variableEntra, valueEntra, fase);
    }

    public void calcularUnoEnFilaPivote() {
        //seteando en 1 el número pivote //fila pivote y columna pivote
        System.out.println("Transformando en 1: " + this.filaPivote + " -- " + this.columnaPivote + ": " + this.modeloConRestricciones.getValueAt(this.filaPivote, this.columnaPivote).toString());
        System.out.println("columna: " + this.modeloConRestricciones.getColumnName(columnaPivote));
        if (!new Fraccion(this.modeloConRestricciones.getValueAt(this.filaPivote, this.columnaPivote).toString()).toString().equals("1")) {
            Fraccion inversoMultiplicativo = new Fraccion(this.modeloConRestricciones.getValueAt(this.filaPivote, this.columnaPivote).toString()).invierte();
            //this.modeloConRestricciones.setValueAt(new Fraccion("1"), filaPivote, columnaPivote);
            for (int j = 2; j < this.columnasRestricciones + 2; j++) {
                Fraccion valorColumaActual = new Fraccion(this.modeloConRestricciones.getValueAt(this.filaPivote, j).toString());
                Fraccion nuevoValorColumnaActual = valorColumaActual.multiplica(inversoMultiplicativo);
                this.modeloConRestricciones.setValueAt(nuevoValorColumnaActual, this.filaPivote, j);
            }
            this.modeloConRestricciones.setValueAt("Fila " + this.filaPivote + " * " + inversoMultiplicativo, this.filaPivote, this.columnaOperacion);
        }
    }

    public void calcularCerosEnColumnaPivote() {
        System.out.println("Calculando Ceros");
        for(int i=1; i<this.modeloConRestricciones.getRowCount() - 2; i++) {
            System.out.println("Fila " + i);
            if(i != this.filaPivote) {
                //se haya el inverso aditivo de la columna que se quiere volver cero
                Fraccion inversoAditivo = new Fraccion(this.modeloConRestricciones.getValueAt(i, this.columnaPivote).toString());
                inversoAditivo.negarSigno();
                System.out.println("inverso Aditivo " + inversoAditivo.toString());
                for(int j=2; j<this.columnasRestricciones + 3; j++) {
                    System.out.println("Columna " + j);
                    //se multiplica el inverso aditivo por la fila pivote
                    Fraccion valorMultiplicado = inversoAditivo.multiplica(new Fraccion(this.modeloConRestricciones.getValueAt(this.filaPivote, j).toString()));
                    System.out.println("valor multiplicado " + valorMultiplicado.toString());
                    Fraccion valorFilaCero = new Fraccion(this.modeloConRestricciones.getValueAt(i, j).toString()).sumar(valorMultiplicado);
                    this.modeloConRestricciones.setValueAt(valorFilaCero, i, j);
                }
                this.modeloConRestricciones.setValueAt("F" + i + " = (F" + filaPivote + " * " + inversoAditivo.toString() + ") + F" + i, i, this.columnaOperacion);
            } else {
                System.out.println("La fila " + i + " es la fila Pivote");
                continue;
            }
        }
    }
    
    public void generarTabla(LinkedHashMap<String, LinkedHashMap<String, Fraccion>> datosTabla, LinkedHashMap<String, Fraccion> funcObjPrimeraFase) {
        Object[] Xb = new Object[datosTabla.get("b").values().size()];
        Object[] Cb = new Object[datosTabla.get("b").values().size()];
        this.modeloConRestricciones.addColumn("Xb", datosTabla.get("Xb").keySet().toArray());
        this.modeloConRestricciones.addColumn("Cb", datosTabla.get("Cb").values().toArray());
        this.modeloConRestricciones.addColumn("b", datosTabla.get("b").values().toArray());
        for (Map.Entry<String, LinkedHashMap<String, Fraccion>> datosVariable : datosTabla.entrySet()) {
            String variable = datosVariable.getKey();
            if(!variable.equals("b") && !variable.equals("Xb") && !variable.equals("Cb")) {
                LinkedHashMap<String, Fraccion> datosFila = datosVariable.getValue();
                this.modeloConRestricciones.addColumn(variable, datosFila.values().toArray());
                this.columnasRestricciones++;
            }
        }
        
        ArrayList variablesFunObj = new ArrayList();
        variablesFunObj.add("");
        variablesFunObj.add("");
        variablesFunObj.add("");
        variablesFunObj.addAll(funcObjPrimeraFase.values());
        this.modeloConRestricciones.insertRow(0, variablesFunObj.toArray());
        
        this.columnaRatio = this.modeloConRestricciones.getColumnCount();
        this.modeloConRestricciones.addColumn("Ratio");
        System.out.println("Columa Ratio: " + this.columnaRatio);
        
        this.columnaOperacion = this.modeloConRestricciones.getColumnCount();
        this.modeloConRestricciones.addColumn("Operacion");
        System.out.println("Columa Operacion: " + this.columnaOperacion);
    }

    public void obtenerColumnasABorrar() {
        Vector columnasSimplificado = new Vector();
        int contadorVariablesAdicionales = 0;
        for(int columna=0; columna<this.modeloConRestricciones.getColumnCount(); columna++) {
            if(!this.modeloConRestricciones.getColumnName(columna).contains("A")) {
                columnasSimplificado.add(this.modeloConRestricciones.getColumnName(columna));
            } else {
                contadorVariablesAdicionales++;
            }
        }
        DefaultTableModel modeloSimplificado = new DefaultTableModel(columnasSimplificado, 0);
        
        for(int fila=0; fila<this.modeloConRestricciones.getRowCount(); fila++) {
            Vector<Object> data = new Vector();
            for(int columna=0; columna<this.modeloConRestricciones.getColumnCount(); columna++) {
                if(!this.modeloConRestricciones.getColumnName(columna).contains("A")) {
                    if(this.modeloConRestricciones.getColumnName(columna).equals("Ratio") || this.modeloConRestricciones.getColumnName(columna).equals("Operacion")) {
                        data.add("");
                    } else {
                        Object dato = this.modeloConRestricciones.getValueAt(fila, columna);                    
                        data.add(dato);
                    }
                }
            }
            modeloSimplificado.addRow(data);
        }
        System.out.println("Columnas " + modeloSimplificado.getColumnCount() + " -- " + modeloSimplificado.getRowCount());
        this.modeloConRestricciones = modeloSimplificado;
        this.columnaOperacion = this.modeloConRestricciones.getColumnCount() - 1;
        this.columnaRatio = this.modeloConRestricciones.getColumnCount() - 2;
        this.columnasRestricciones -= contadorVariablesAdicionales;
        System.out.println("Columnas Resultantes " + this.columnasRestricciones);
    }
    
     public void generarTablaSegundaFase(Vector<Fraccion> coeficientesFuncObj) {
        int columna = 3;
        for (Fraccion coeficiente : coeficientesFuncObj) {
            this.modeloConRestricciones.setValueAt(coeficiente, 0, columna);
            String columnaEscogida = this.modeloConRestricciones.getColumnName(columna);
            for(int fila=1; fila<this.modeloConRestricciones.getRowCount() - 2; fila++) {
                if(columnaEscogida.equals(this.modeloConRestricciones.getValueAt(fila, 0).toString())) {
                    this.modeloConRestricciones.setValueAt(coeficiente, fila, 1);
                }
            }
            columna++;
        }
        this.obtenerColumnasABorrar();
    }
    
    public boolean iterarPrimeraFase(DosFasesFrame frame) {
        this.iteracion++;
        return this.minimizar(frame, "MIN", "I");
    }
    
    public boolean iterarSegundaFase(DosFasesFrame frame, String metodo) {
        this.iteracion++;
        if(metodo.equals("MAX")) {
            return this.maximizar(frame, metodo, "II");
        } else {
            return this.minimizar(frame, metodo, "II");
        }
    }
    
    public boolean minimizar(DosFasesFrame frame, String metodo, String fase) {
        frame.setLabelDosFasesTbl("Iteración " + this.iteracion);
        this.calcularZj();
        Fraccion z = calcularZ();
        boolean tienePositivos = verificarZjCjPositivos();
        this.modeloConRestricciones.setValueAt("Z = " + z.toString(), 0, 0);
        if(tienePositivos) {
            this.hayarColumnaPivote(metodo);
            this.hayarFilaPivote(this.columnaPivote, fase);
            frame.resaltar(this.filaPivote, this.columnaPivote, fase);
            return true;
        } else {
            String variablesEncontradas = "";
            for(int fila=1; fila<this.modeloConRestricciones.getRowCount() - 2; fila++) {
                String variable = this.modeloConRestricciones.getValueAt(fila, 0).toString();
                String coeficiente = this.modeloConRestricciones.getValueAt(fila, 2).toString();
                variablesEncontradas += variable + " = " + coeficiente + ", \t";
            }
            frame.detenerDosFases(variablesEncontradas, fase);
            return false;
        }
    }
    
    public boolean maximizar(DosFasesFrame frame, String metodo, String fase) {
        frame.setLabelDosFasesTbl("Iteración " + this.iteracion);
        this.calcularZj();
        Fraccion z = calcularZ();
        this.modeloConRestricciones.setValueAt("Z = " + z.toString(), 0, 0);
        boolean tieneNegativos = verificarZjCjNegativos();
        if(tieneNegativos) {
            this.hayarColumnaPivote(metodo);
            this.hayarFilaPivote(this.columnaPivote, fase);
            frame.resaltar(this.filaPivote, this.columnaPivote, fase);
            return true;
        } else {
            String variablesEncontradas = "";
            for(int fila=1; fila<this.modeloConRestricciones.getRowCount() - 2; fila++) {
                String variable = this.modeloConRestricciones.getValueAt(fila, 0).toString();
                String coeficiente = this.modeloConRestricciones.getValueAt(fila, 2).toString();
                variablesEncontradas += variable + " = " + coeficiente + ", \t";
            }
            frame.detenerDosFases(variablesEncontradas, fase);
            return false;
        }
    }
    
    /**
     * Se verifica que los valores de Zj - Cj sean positivos (para minimización)
     * si hay un positivo(true)... se sigue iterando
     * 
     * @return 
     */
    private boolean verificarZjCjPositivos() {
        boolean resp = false;
        for(int columna=3; columna<this.modeloConRestricciones.getColumnCount() - 2; columna++) {
            Fraccion ZjCj = new Fraccion(this.modeloConRestricciones.getValueAt(this.modeloConRestricciones.getRowCount() -1, columna).toString());
            if(ZjCj.esMayorQue(0.0)) {
                resp = true;
                break;
            }
        }
        return resp;
    }
    
    /**
     * Se verifica que los valores de Zj - Cj sean negativos (para maximización)
     * si hay un negativo(true)... se sigue iterando
     * 
     * @return 
     */
    private boolean verificarZjCjNegativos() {
        boolean resp = false;
        for(int columna=3; columna<this.modeloConRestricciones.getColumnCount() - 2; columna++) {
            Fraccion ZjCj = new Fraccion(this.modeloConRestricciones.getValueAt(this.modeloConRestricciones.getRowCount() -1, columna).toString());
            if(ZjCj.esMenorQue(0.0)) {
                resp = true;
                break;
            }
        }
        return resp;
    }
    
    public Fraccion calcularZ() {
        Fraccion z = new Fraccion("0");
        for(int fila=1; fila<this.modeloConRestricciones.getRowCount()-2; fila++) {
            Fraccion Cb = new Fraccion(this.modeloConRestricciones.getValueAt(fila, 1).toString());
            Fraccion b = new Fraccion(this.modeloConRestricciones.getValueAt(fila, 2).toString());
            Fraccion multiplica = Cb.multiplica(b);
            z = z.sumar(multiplica);
        }
        return z;
        
        /*int filaZjCj = this.modeloConRestricciones.getRowCount() - 1;
        int contadorNegativos = 0;
        for(int columna=3; columna<this.modeloConRestricciones.getColumnCount()-2; columna++) {
            System.out.println("Columna: " + this.modeloConRestricciones.getColumnName(columna) + ": " + this.modeloConRestricciones.getValueAt(filaZjCj, columna).toString()+ " -- " + ((Fraccion) this.modeloConRestricciones.getValueAt(filaZjCj, columna)).getSigno());
            if(new Fraccion(this.modeloConRestricciones.getValueAt(filaZjCj, columna).toString()).getSigno() == -1) {
                contadorNegativos++;
            }
        }
        
        return (contadorNegativos != 0);*/
    }
    
    public DefaultTableModel getModelConRestricciones() {
        return this.modeloConRestricciones;
    }
}
