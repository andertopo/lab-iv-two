/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util.textelements;

import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Vector;
import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import util.Fraccion;

/**
 *
 * @author atorrepo
 */
public class Formalizador {
    
    public String obtenerMetodoEscogido(ButtonGroup btnGrpOpciones) {
        String texto = "";
        while (btnGrpOpciones.getElements().hasMoreElements()) {
            JRadioButton rbtn = (JRadioButton) btnGrpOpciones.getElements().nextElement();
            if (rbtn.isSelected()) {
                texto = rbtn.getText();
                break;
            } else {
                texto = "MIN";
                rbtn.setText("MIN");
                break;                
            }            
        }
        return texto;
    }
    
    public String obtenerFuncionObjetivo(Vector<Fraccion> funcObjetivo) {
        String texto = "";
        int contadorCoeficientes = 1;
        for (Fraccion coeficiente : funcObjetivo) {
            texto += (contadorCoeficientes > 1 && !coeficiente.esMenorQue(0.0)) ? " + " : "";
            texto += coeficiente.toString(true) + "X" + contadorCoeficientes;
            contadorCoeficientes++;
        }
        return texto;
    }
    
    public String getRestricciones(DefaultTableModel model) {
        String texto = "";
        for (int i = 0; i < model.getRowCount(); i++) {
            for (int j = 0; j < model.getColumnCount(); j++) {
                String value = model.getValueAt(i, j).toString();
                if (j > 0 && (model.getColumnName(j).equals("X1") || model.getColumnName(j).equals("X2")) && (value.indexOf("+") == -1 && value.indexOf("-") == -1)) {
                    value = "+ " + value;
                }
                
                texto += value;
                if (model.getColumnName(j).equals("X1") || model.getColumnName(j).equals("X2")) {
                    texto += model.getColumnName(j) + " ";
                } else {
                    texto += " ";
                }
            }
            texto += "\n";
        }
        return texto;
    }
    
    public String getRestriccionesDosFases(JTable tabla) {
        String texto = "";
        DefaultTableModel model = (DefaultTableModel) tabla.getModel();
        int columnaDesigualdad = tabla.getColumn("Desigualdad").getModelIndex();
        int columnaCondicion = tabla.getColumn("Condicion").getModelIndex();
        for (int i = 0; i < model.getRowCount(); i++) {
            for (int j = 0; j < model.getColumnCount(); j++) {
                String value = (model.getValueAt(i, j) != null && !model.getValueAt(i, j).equals("")) ? model.getValueAt(i, j).toString() : "0";
                if (j == columnaDesigualdad || j == columnaCondicion) {
                    texto += " " + value;
                } else {
                    Fraccion coeficienteRestriccion = new Fraccion(value);
                    if (j > 0) {
                        texto += (!coeficienteRestriccion.esMenorQue(0.0)) ? " + " + coeficienteRestriccion : coeficienteRestriccion.toString(true);
                    } else {
                        texto += value;
                    }
                }
                texto += (!model.getColumnName(j).equals("Desigualdad") && !model.getColumnName(j).equals("Condicion")) ? model.getColumnName(j) : "";
            }
            texto += "\n";
        }
        return texto;
    }
    
    private LinkedHashMap<String, LinkedHashMap<String, Fraccion>> construirDatosFuncObj(Vector<Fraccion> funcObjetivo) {
        LinkedHashMap<String, LinkedHashMap<String, Fraccion>> funcionesObjetivo = new LinkedHashMap<String, LinkedHashMap<String, Fraccion>>();
        
        LinkedHashMap<String, Fraccion> funcionObjetivoCompleta = new LinkedHashMap<>();
        LinkedHashMap<String, Fraccion> funcionObjetivoPrimeraFase = new LinkedHashMap<>();
        int i = 1;
        for (Fraccion coeficiente : funcObjetivo) {
            funcionObjetivoCompleta.put("X" + i, coeficiente);
            funcionObjetivoPrimeraFase.put("X" + i, new Fraccion("0"));
            i++;
        }
        funcionesObjetivo.put("funcObjCompleta", funcionObjetivoCompleta);
        funcionesObjetivo.put("funcObjPrimeraFase", funcionObjetivoPrimeraFase);
        
        return funcionesObjetivo;
    }
    
    public LinkedHashMap<String, LinkedHashMap<String, Fraccion>> getFuncionObjetivoDosFases(Vector<Fraccion> funcObjetivo, JTable tabla, ButtonGroup btnGrpOpciones) {
        LinkedHashMap<String, LinkedHashMap<String, Fraccion>> funcionesObjetivo = this.construirDatosFuncObj(funcObjetivo);
                
        DefaultTableModel model = (DefaultTableModel) tabla.getModel();
        int columnaDesigualdad = tabla.getColumn("Desigualdad").getModelIndex();
        int contadorH = 1;
        int contadorS = 1;
        int contadorA = 1;
        for (int i = 0; i < model.getRowCount(); i++) {
            String desigualdad = model.getValueAt(i, columnaDesigualdad).toString();
            if(desigualdad.equals("<=")) {
                funcionesObjetivo.get("funcObjCompleta").put("H" + contadorH, new Fraccion("0"));
                funcionesObjetivo.get("funcObjPrimeraFase").put("H" + contadorH, new Fraccion("0"));
                contadorH++;
            } else if(desigualdad.equals(">=")) {
                funcionesObjetivo.get("funcObjCompleta").put("S" + contadorS, new Fraccion("0"));
                funcionesObjetivo.get("funcObjPrimeraFase").put("S" + contadorS, new Fraccion("0"));

                //funcionesObjetivo.get("funcObjCompleta").put("A" + contadorA, (this.obtenerMetodoEscogido(btnGrpOpciones).equals("MAX") ? new Fraccion("-1") : new Fraccion("1")));
                //funcionesObjetivo.get("funcObjPrimeraFase").put("A" + contadorA, (this.obtenerMetodoEscogido(btnGrpOpciones).equals("MAX") ? new Fraccion("-1") : new Fraccion("1")));
                funcionesObjetivo.get("funcObjCompleta").put("A" + contadorA, (new Fraccion("1")));
                funcionesObjetivo.get("funcObjPrimeraFase").put("A" + contadorA, (new Fraccion("1")));

                contadorS++;
                contadorA++;
            } else if(desigualdad.equals("=")) {
                //funcionesObjetivo.get("funcObjCompleta").put("A" + contadorA, (this.obtenerMetodoEscogido(btnGrpOpciones).equals("MAX") ? new Fraccion("-1") : new Fraccion("1")));
                //funcionesObjetivo.get("funcObjPrimeraFase").put("A" + contadorA, (this.obtenerMetodoEscogido(btnGrpOpciones).equals("MAX") ? new Fraccion("-1") : new Fraccion("1")));
                funcionesObjetivo.get("funcObjCompleta").put("A" + contadorA, (new Fraccion("1")));
                funcionesObjetivo.get("funcObjPrimeraFase").put("A" + contadorA, (new Fraccion("1")));
                contadorA++;
            }
        }
        System.out.println("jo jo jo " + funcionesObjetivo.toString());
        return funcionesObjetivo;
    }
    
    public String getFuncObjCompletaText(LinkedHashMap<String, Fraccion> funcionObjetivoCompleta) {
        String texto = "";
        int i=0;
        for (Map.Entry<String, Fraccion> coeficiente : funcionObjetivoCompleta.entrySet()) {
            String variable = coeficiente.getKey();
            Fraccion coeficienteVariable = coeficiente.getValue();
            if(i!= 0) {
                texto += (coeficienteVariable.getSigno() != -1) ? " + " : "";
                texto += coeficienteVariable.toString(true) + variable;
            } else {
                texto += coeficienteVariable.toString(true) + variable;
                i++;
            }
        }
        System.out.println("TEXTO: " + texto);
        return texto;
    }
    
    public LinkedHashMap<String, LinkedHashMap<String, Fraccion>> getTabla(LinkedHashMap<String, Fraccion> funcionObjetivoPrimeraFase, JTable tabla, ButtonGroup btnGrpOpciones) {
        DefaultTableModel model = (DefaultTableModel) tabla.getModel();
        LinkedHashMap<String, LinkedHashMap<String, Fraccion>> datosTabla = new LinkedHashMap();
        int contadorH = 1;
        int contadorS = 1;
        int contadorA = 1;
        for (Map.Entry<String, Fraccion> entry : funcionObjetivoPrimeraFase.entrySet()) {
            String variable = entry.getKey();
            LinkedHashMap<String, Fraccion> datosFilas = new LinkedHashMap();
            for(int fila=0; fila<model.getRowCount(); fila++) {
                try {
                    int columna = tabla.getColumn(variable).getModelIndex();
                    String valorAInsertar = (model.getValueAt(fila, columna) != null) ? model.getValueAt(fila, columna).toString() : "0";
                    datosFilas.put("fila_" + (fila + 1), new Fraccion(valorAInsertar));
                    System.out.println("Modelo inidice " + variable + " - fila " + fila + ": " + columna);
                } catch(java.lang.IllegalArgumentException e) {
                    System.out.println("No encuentra la columna " + variable + " es de las de holgura o adicional");
                    datosFilas.put("fila_" + (fila + 1), new Fraccion("0"));
                }
            }
            datosTabla.put(variable, datosFilas);
        }
        datosTabla.put("b", new LinkedHashMap<>());
        datosTabla.put("Xb", new LinkedHashMap<>());
        datosTabla.put("Cb", new LinkedHashMap<>());
        int columnaDesigualdad = tabla.getColumn("Desigualdad").getModelIndex();
        int columnaCondicion = tabla.getColumn("Condicion").getModelIndex();
        
        for(int fila=0; fila<model.getRowCount(); fila++) {
            if(model.getValueAt(fila, columnaDesigualdad).equals("<=")) {
                datosTabla.get("H" + contadorH).put("fila_" + (fila + 1), new Fraccion("1"));

                datosTabla.get("Xb").put("H" + contadorH, new Fraccion("" + fila));
                datosTabla.get("Cb").put("fila_" + (fila + 1), new Fraccion("0"));

                contadorS++;
            } else if(model.getValueAt(fila, columnaDesigualdad).equals(">=")) {
                datosTabla.get("S" + contadorS).put("fila_" + (fila + 1), new Fraccion("-1"));
                datosTabla.get("A" + contadorA).put("fila_" + (fila + 1), new Fraccion("1"));

                datosTabla.get("Xb").put("A" + contadorA, new Fraccion("" + fila));
                datosTabla.get("Cb").put("fila_" + (fila + 1), new Fraccion("1"));
                contadorS++;
                contadorA++;
            } else {
                datosTabla.get("A" + contadorA).put("fila_" + (fila + 1), new Fraccion("1"));
                datosTabla.get("Xb").put("A" + contadorA, new Fraccion("" + fila));
                datosTabla.get("Cb").put("fila_" + (fila + 1), new Fraccion("1"));
                contadorA++;
            }

            String valorAInsertar = (model.getValueAt(fila, columnaCondicion) != null) ? model.getValueAt(fila, columnaCondicion).toString() : "0";
            datosTabla.get("b").put("fila_" + (fila + 1), new Fraccion(valorAInsertar));
            System.out.println("datos tabla en fila"  + fila + " -- " + datosTabla.toString());
        }
        System.out.println("datos tabla " + datosTabla.toString());
        return datosTabla;
    }
    
    public String getRestriccionesCompletasText(LinkedHashMap<String, LinkedHashMap<String, Fraccion>> datosTabla, JTable tabla) {
        String texto = "";
        int columnaDesigualdad = tabla.getColumn("Desigualdad").getModelIndex();
        for(int fila = 0; fila<datosTabla.get("b").size(); fila++) {
            for (Map.Entry<String, LinkedHashMap<String, Fraccion>> variableData : datosTabla.entrySet()) {
                String variable = variableData.getKey();
                LinkedHashMap<String, Fraccion> datosFila = variableData.getValue();
                if(!variable.equals("b") && !variable.equals("Xb") && !variable.equals("Cb")) {
                    if(!datosFila.get("fila_" + (fila + 1)).toString().equals("0")) {
                        texto += (!variable.equals("X1") && datosFila.get("fila_" + (fila + 1)).getSigno() != -1) ? " + " : "";
                        texto += datosFila.get("fila_" + (fila + 1)).toString(true) + variable;
                    }
                } else {
                    if(variable.equals("b")) {
                        texto += " " + tabla.getValueAt(fila, columnaDesigualdad) + " ";
                        texto += datosFila.get("fila_" + (fila + 1)).toString(true) + "\n";
                    }
                }
            }
        }
        System.out.println(texto);
        return texto;
    }
}
