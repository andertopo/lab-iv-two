package util;

public class Fraccion {

    private int signo;
    private int numerador;
    private int denominador;

    public Fraccion(int n, int d) throws NumberFormatException {
        try {
           if (d == 0) {
            System.out.println("Una fracción no puede tener como denominador el número 0");
            throw new NumberFormatException("Una fracción no puede tener como denominador el número 0");
        } else {
            if(n == 0) {
                d = 1;
                signo = 1;
            }
            this.validarSigno(n, d);
            this.numerador = Math.abs(n);
            this.denominador = Math.abs(d);
        }  
        } catch (Exception e) {
            System.err.println("Fraccion: " + e.toString());
        }
       
    }

    public Fraccion(String fraccion) throws NumberFormatException {
        if (fraccion.indexOf("/") == -1) {
            //throw new NumberFormatException("Fraccionario no valido, por favor usar / ej: 2/5");
            fraccion = fraccion.replaceAll(" ", "");
            this.validarSigno(Integer.parseInt(fraccion), 1);
            this.setNumerador(Math.abs(Integer.parseInt(fraccion)));
            this.setDenominador(1);
        } else {
            fraccion = fraccion.replaceAll(" ", "");
            String[] fraccionario = fraccion.split("/");
            this.validarSigno(Integer.parseInt(fraccionario[0]), Integer.parseInt(fraccionario[1]));
            this.setNumerador(Math.abs(Integer.parseInt(fraccionario[0])));
            this.setDenominador(Math.abs(Integer.parseInt(fraccionario[1])));
        }
        if(this.numerador == 0) {
            this.denominador = 1;
            this.signo = 1;
        }
    }
    
    public Fraccion() {}

    /**
     * verifica si la fracción es negativa
     * 
     * @param n
     * @param d 
     */
    public void validarSigno(int n, int d) {
        if (n * d < 0) {
            this.signo = -1;
        } else {
            this.signo = 1;
        }
    }
    
    /**
     * cambia el signo de la fraccion
     */
    public void negarSigno() {
        this.signo *= -1;
    }

    public void setNumerador(int numerador) {
        this.numerador = numerador;
    }

    public void setDenominador(int denominador) {
        this.denominador = denominador;
    }

    int getNumerador() {
        return this.numerador;
    }

    int getDenominador() {
        return this.denominador;
    }
    
    public int getSigno() {
        return this.signo;
    }
    
    /**
     * Obtiene el valor Double de la fraccion
     * 
     * @return valor en Double
     */
    public Double parseDouble() {
        if (signo == -1) {
            return ((new Integer(numerador).doubleValue() / new Integer(denominador).doubleValue()) * -1);
        } else {
            return (new Integer(numerador).doubleValue() / new Integer(denominador).doubleValue());
        }
    }

    /**
     * Compara si el fraccionario es menor que el Double enviado
     * 
     * @param valorAComparar comparativo
     * @return true si es menor, false en caso contrario
     */
    public boolean esMenorQue(Double valorAComparar) {
        return this.parseDouble() < valorAComparar;
    }
    
    /**
     * Compara si el fraccionario es menor que el Double enviado
     * 
     * @param valorAComparar comparativo
     * @return true si es menor, false en caso contrario
     */
    public boolean esMenorQue(Fraccion valorAComparar) {
        return this.parseDouble() < valorAComparar.parseDouble();
    }
    
    public boolean esMenorIgualQue(Double valorAComparar) {
        return this.parseDouble() <= valorAComparar;
    }
    
    public boolean esMenorIgualQue(Fraccion valorAComparar) {
        return this.parseDouble() <= valorAComparar.parseDouble();
    }
    
    /**
     * Compara si el fraccionario es mayor que el Double enviado
     * 
     * @param valorAComparar comparativo
     * @return true si es mayor, false en caso contrario
     */
    public boolean esMayorQue(Double valorAComparar) {
        return this.parseDouble() > valorAComparar;
    }
    
    /**
     * Compara si el fraccionario es mayor que el Double enviado
     * 
     * @param valorAComparar comparativo
     * @return true si es mayor, false en caso contrario
     */
    public boolean esMayorQue(Fraccion valorAComparar) {
        return this.parseDouble() > valorAComparar.parseDouble();
    }
    
    public boolean esMayorIgualQue(Double valorAComparar) {
        return this.parseDouble() >= valorAComparar;
    }
    
    public boolean esMayorIgualQue(Fraccion valorAComparar) {
        return this.parseDouble() >= valorAComparar.parseDouble();
    }
    
    public String toString(boolean pretty) {
        String texto = "" + numerador;
        if (signo == -1) {
            texto = " - " + this.numerador;
        }
        if (denominador != 1) {
            texto += "/" + this.denominador;
        }
        return texto;
    }

    public String toString() {
        String texto = "" + numerador;
        if (signo == -1) {
            texto = "-" + this.numerador;
        }
        if (denominador != 1) {
            texto += "/" + this.denominador;
        }
        return texto;
    }
    
    /**
     * Devuelve una fracción que es el resultado de sumar la fracción
     * original con otra fracción que se pasa como parámetro.
     * 
     * @param b Fraccion que se pasa como parametro
     * @return Fraccion que da como resultado la suma de esta fraccion mas la 
     * pasada por parametro
     */
    public Fraccion sumar(Fraccion b){
        Fraccion c = new Fraccion(((this.numerador * this.signo) * b.getDenominador()) + ((b.getNumerador() * b.getSigno()) * this.denominador), this.denominador * b.getDenominador());
        c = c.simplifica();
        return c;
    }
    
    /**
     * Devuelve una fracción que es el resultado de restar la fracción
     * original con otra fracción que se pasa como parámetro.
     * 
     * @param b Fraccion que se pasa como parametro
     * @return Fraccion que da como resultado la resta de esta fraccion mas la 
     * pasada por parametro
     */
    public Fraccion restar(Fraccion b){
        Fraccion c = new Fraccion(((this.signo * this.numerador) * b.getDenominador()) - ((b.getSigno() * b.getNumerador()) * this.denominador), this.denominador * b.getDenominador());
        c = c.simplifica();
        return c;
    }

    /**
     * Devuelve una fracción invertida. Lo que antes era el numerador ahora será
     * el denominador y viceversa.
     *
     * @return fracción invertida
     */
    public Fraccion invierte() {
        return new Fraccion(this.signo * this.denominador, this.numerador);
    }

    /**
     * Devuelve una fracción multiplicada por un escalar (un número)
     * <code>n</code>.
     * <p>
     * Cuando una fracción se multiplica por un número <code>n</code>, el
     * resultado es otra fracción con el mismo denominador que la original. El
     * numerador se obtiene multiplicando <code>n</code> por el numerador de la
     * fracción original.
     *
     * @param n escalar por el que se multiplica la fracción original
     * @return fracción multiplicada por <code>n</code>
     */
    public Fraccion multiplica(int n) {
        Fraccion c = new Fraccion(this.signo * this.numerador * n, this.denominador);
        c = c.simplifica();
        return c;
    }

    /**
     * Devuelve una fracción que es el resultado de multiplicar la fracción
     * original por otra fracción que se pasa como parámetro.
     * <p>
     * Cuando se multiplican dos fracciones, el numerador de la fracción
     * resultante es el resultado de multiplicar los numeradores de las dos
     * fracciones. El denominador de la fracción resultante se calcula de forma
     * análoga.
     *
     * @param f fracción por la que se multiplica la fracción original
     * @return resultado de multiplicar la fracción original por la fracción que
     * se pasa como parámetro
     */
    public Fraccion multiplica(Fraccion f) {
        Fraccion c = new Fraccion(this.signo * this.numerador * f.getNumerador(), this.denominador * (f.getDenominador() * f.getSigno()));
        c = c.simplifica();
        return c;
    }

    /**
     * Devuelve una fracción dividida entre un escalar (un número) <code>n
     * </code>.
     * <p>
     * Cuando una fracción se divide entre un número <code>n</code>, el
     * resultado es otra fracción con el mismo numerador que la original. El
     * denominador se obtiene multiplicando <code>n</code> por el denominador de
     * la fracción original.
     *
     * @param n escalar entre el que se divide la fracción original
     * @return fracción dividida entre <code>n</code>
     */
    public Fraccion divide(int n) {
        Fraccion c = new Fraccion(this.signo * this.numerador, this.denominador * n);
        c = c.simplifica();
        return c;
    }

    /**
     * Devuelve una fracción que es el resultado de dividir la fracción original
     * entre otra fracción que se pasa como parámetro.
     * <p>
     * Para obtener la división de dos fracciones, el numerador de una fracción
     * se multiplica por el denominador de otra y viceversa.
     *
     * @param f fracción entre la que se quiere dividir la fracción original
     * @return resultado de dividir la fracción original entre la fracción que
     * se pasa como parámetro
     */
    public Fraccion divide(Fraccion f) {
        Fraccion c = new Fraccion(this.signo * this.numerador * f.getDenominador(), denominador * f.getNumerador());
        c = c.simplifica();
        return c;
    }

    /**
     * Devuelve una fracción que es el resultado de simplificar la fracción
     * original.
     * <p>
     * Para simplificar una fracción, se comprueba si numerador y denominador
     * son divisibles entre el mismo número. En tal caso, los dos se dividen. Se
     * repite el proceso hasta que la fracción que se obtiene es irreducible (no
     * se puede simplificar más).
     *
     * @return resultado de simplificar (si se puede) la fracción original, o la
     * misma fracción en caso de que la original sea irreducible
     */
    public Fraccion simplifica() {

        int s = this.signo;
        int n = this.numerador;
        int d = this.denominador;

        for (int i = 2; i <= Math.min(this.numerador, this.denominador); i++) {
            while (((n % i) == 0) && ((d % i) == 0)) {
                n /= i;
                d /= i;
            }
        }

        return new Fraccion(s * n, d);
    }
}
