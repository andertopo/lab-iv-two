package util.drawelements;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author atorrepo
 */
/**
* Applied background and foreground color to single column of a JTable
* in order to distinguish it apart from other columns.
*/
public class ColorColumnRenderer extends DefaultTableCellRenderer
{
   Color bkgndColor, fgndColor;
   int filaPivote;
   int columnaPivote;
     
   public ColorColumnRenderer(Color bkgnd, Color foregnd, int filaPivote, int columnaPivote) {
      super();
      bkgndColor = bkgnd;
      fgndColor = foregnd;
      this.filaPivote = filaPivote;
      this.columnaPivote = columnaPivote;
   }
     
   public Component getTableCellRendererComponent
        (JTable table, Object value, boolean isSelected,
         boolean hasFocus, int row, int column)
   {
      Component cell = super.getTableCellRendererComponent
         (table, value, isSelected, hasFocus, row, column);
      if(column == columnaPivote) {
        //System.out.println(row + " Columna " + column + " columna-pivote: " + columnaPivote);
        cell.setBackground( bkgndColor );
        if(row == filaPivote) {
            cell.setForeground( Color.ORANGE );
        } else {
            cell.setForeground( fgndColor );
        }
      } else {
        //System.out.println("No es Columna Pivote " + column);
        if(row == filaPivote) {
          //System.out.println("Fila " + row + " fila pivote: " + filaPivote);
          cell.setBackground( bkgndColor );
        } else {
          cell.setBackground( null );  
        }
        cell.setForeground( fgndColor );
      }
      
      return cell;
   }
}