/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util.drawelements;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

/**
 *
 * @author atorrepo
 */
public class Grafica {
    
    public JFreeChart getGrafica(DefaultTableModel model) {
        XYSeriesCollection dataset = new XYSeriesCollection();
        for(int i=0; i<model.getRowCount(); i++) {
            dataset.addSeries(getCoordenadas((i + 1), (Double) model.getValueAt(i, 0), (Double) model.getValueAt(i, 1), (Double) model.getValueAt(i, 3)));
        }
        return ChartFactory.createXYLineChart("Metodo Grafico","","",dataset,PlotOrientation.HORIZONTAL,true,true,true);
    }
    
    public XYSeries getCoordenadas(int i, Double x1, Double x2, Double res) {
        XYSeries serie = new XYSeries("Linea " + i);
        serie.add(0.0, res/x1);
        serie.add(res/x2, 0.0);
        return serie;
    }
    

}
