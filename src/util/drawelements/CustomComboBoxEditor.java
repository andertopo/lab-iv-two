/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util.drawelements;

import java.awt.Component;
import javax.swing.DefaultCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JTable;

/**
 *
 * @author atorrepo
 */
public class CustomComboBoxEditor extends DefaultCellEditor {

	private DefaultComboBoxModel model = null;
	
	String values;

	public CustomComboBoxEditor(String val) {
	   super(new JComboBox());
	   this.model = (DefaultComboBoxModel)((JComboBox)getComponent()).getModel();
	   values = val;
	}

	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		model.removeAllElements();
		
		String [] items = values.split(",");
		for(String item: items) {
			model.addElement(item);
		}
		
//	    model.addElement("<=");
//	    model.addElement(">=");
	    return super.getTableCellEditorComponent(table, value, isSelected, row, column);
	} 
	
}